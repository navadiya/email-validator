package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {
	
	private static final String regex = "^([a-z][a-z0-9]{2,})@([a-z0-9]{3,}).([a-z]{2,})";
	public static boolean isValidEmail(String email) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
	    return matcher.matches();
	}
}
