package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidationTest {
	@Test
	public void testIsValidEmailRegular() {
		boolean isValid = EmailValidation.isValidEmail("deep@gmail.com");
		assertTrue( "Invalid Email", isValid);
	}
	@Test
	public void testIsValidEmailException() {
		boolean isValid = EmailValidation.isValidEmail("9deep9@gmail.com");
		assertFalse( "Invalid Email", isValid);
	}
	@Test
	public void testIsValidEmailBoundaryIn() {
		boolean isValid = EmailValidation.isValidEmail("dee@gma.ca");
		assertTrue( "Invalid Email", isValid);
	}
	
	@Test
	public void testIsValidEmailBoundaryOut() {
		boolean isValid = EmailValidation.isValidEmail("de@gmail.com");
		assertFalse( "Invalid Email", isValid);
	}
	
	@Test
	public void testIsValidEmailWithSymbolRegular() {
		boolean isValid = EmailValidation.isValidEmail("deep98@gmail.com");
		assertTrue( "Invalid Email", isValid);
	}
	
	@Test
	public void testIsValidEmailWithSymbolException() {
		boolean isValid = EmailValidation.isValidEmail("deep@navadiya98@gmail.com");
		assertFalse( "Invalid Email", isValid);
	}
	
	@Test
	public void testIsValidEmailWithAccountRegular() {
		boolean isValid = EmailValidation.isValidEmail("deep98@gmail.com");
		assertTrue( "Invalid Account", isValid);	
	}
	
	@Test
	public void testIsValidEmailWithAccountException() {
		boolean isValid = EmailValidation.isValidEmail("98deep@gmail.com");
		assertFalse( "Invalid Account", isValid);	
	}
	
	@Test
	public void testIsValidEmailWithDomainRegular() {
		boolean isValid = EmailValidation.isValidEmail("deep98@gmail.com");
		assertTrue( "Invalid Domain", isValid);	
	}

	@Test
	public void testIsValidEmailWithDomainExecption() {	
		boolean isValid = EmailValidation.isValidEmail("deep98@Gmail.com");
		assertFalse( "Invalid Domain", isValid);
	}
	
	@Test
	public void testIsValidEmailWithDomainBoundaryIn() {
		boolean isValid = EmailValidation.isValidEmail("deep98@gma.com");
		assertTrue( "Invalid Domain", isValid);
	}
	@Test
	public void testIsValidEmailWithDomainBoundaryOut() {
		boolean isValid = EmailValidation.isValidEmail("deep98@Gmail9898.com");
		assertFalse( "Invalid Domain", isValid);
	}

	@Test
	public void testIsValidEmailWithExtensionRegular() {
		boolean isValid = EmailValidation.isValidEmail("deep98@gmail.com");
		assertTrue( "Invalid Extension", isValid);
		
		}
	@Test
	public void testIsValidEmailWithExtensionExecption() {
		boolean isValid = EmailValidation.isValidEmail("deep98@gmail.c");
		assertFalse( "Invalid Extension", isValid);
		}
	@Test
	public void testIsValidEmailWithExtensionBoundaryIn() {
		boolean isValid = EmailValidation.isValidEmail("deep98@gmail.ca");
		assertTrue( "Invalid Extension", isValid);
	}
}
